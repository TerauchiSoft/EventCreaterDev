﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreater
{
    /// <summary>
    /// Listデータをあれこれ
    /// </summary>
    public static class CollectionTool
    {
        /// <summary>
        /// イベントの数をロード
        /// </summary>
        /// <returns></returns>
        public static int GetEventNum(List<string> strs)
        {
            return int.Parse(strs[0].Split(':')[1]);
        }

        /// <summary>
        /// メタデータを省く
        /// </summary>
        /// <param name="eventNum"></param>
        /// <param name="strs"></param>
        /// <returns></returns>
        private static List<string> GetEventContents(int eventNum, List<string> strs)
        {
            strs.RemoveRange(0, eventNum + 1);
            return strs;
        }

        /// <summary>
        /// ファイルの文字データからイベントタイトルを抜き出す。
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetEventTitle(int eventNum, string[] ardata, string label)
        {
            List<string> data = new List<string>();
            data.AddRange(ardata);
            data = GetEventContents(eventNum, data);
            int idx = GetIndexLabel(data, label + ":");
            if (idx == -1)
            {
                MessageBox.Show("イベントのタイトルが見つかりません。",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return null;
            }
            else
            {
                string title = data[idx].Split(':')[1];
                return title;
            }
        }

        /// <summary>
        /// ファイルの文字データから出現条件を抜き出す。
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<string> GetAppearCondData(int eventNum, string[] ardata, string label)
        {
            List<string> data = new List<string>();
            data.AddRange(ardata);
            data = GetEventContents(eventNum, data);
            int idx = GetIndexLabel(data, label + ":");
            int endidx = GetIndexLabel(data, "Event content", idx);
            if (idx == -1 || endidx == -1)
            {
                MessageBox.Show("イベントの出現条件が見つかりません。",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return null;
            }
            else
            {
                idx += 3;
                endidx -= 2;
                return GetListDataByRet(data, idx, endidx);
            }
        }

        /// <summary>
        /// ファイルの文字データからイベントコンテンツを抜き出す。
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<string> GetEventContentsData(int eventNum, string[] ardata, string label)
        {
            List<string> data = new List<string>();
            data.AddRange(ardata);
            data = GetEventContents(eventNum, data);
            int idx = GetIndexLabel(data, label + ":");
            int endidx = GetIndexLabel(data, "Event content", idx);
            if (idx == -1 || endidx == -1)
            {
                MessageBox.Show("イベントコンテンツが見つかりません。",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return null;
            }
            else
            {
                idx = endidx + 2;
                endidx = GetIndexLabel(data, "}", idx) - 1;
                if (idx != -1 || endidx != -1)
                {
                    List<string> rets = GetListDataByRet(data, idx, endidx);
                    // メタ行を消す。
                    if (rets[rets.Count - 1] == "") rets.RemoveAt(rets.Count - 1);
                    return rets;
                }
                else
                {
                    MessageBox.Show("イベントコンテンツの境界が曖昧です。",
                                    "エラー",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                    return null;
                }
            }
        }

        /// <summary>
        /// List<string>データの範囲を抜き出す
        /// </summary>
        /// <param name="strs"></param>
        /// <param name="idx"></param>
        /// <param name="eidx"></param>
        /// <returns></returns>
        private static List<string> GetListDataByRet(List<string> strs, int idx, int eidx)
        {
            List<string> retdata = new List<string>();
            for (int i = idx; i <= eidx; i++)
            {
                // strs[i] = RemoveChar(strs[i], '\t');
                retdata.Add(strs[i]);
            }
            return retdata;
        }

        /// <summary>
        /// 特定の文字を全て消去する。
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RemoveChar(string str, char del)
        {
            string[] strs = str.Split(del);
            string rstr = "";
            foreach (var a in strs)
                rstr += a;
            return rstr;
        }

        /// <summary>
        /// ラベルと一致するインデックスを返す。
        /// ない場合は-1を返す。
        /// </summary>
        /// <param name="data"></param>
        /// <param name="lblname"></param>
        /// <returns></returns>
        public static int GetIndexLabel(List<string> data, string lblname, int startFind = 0, int endFind = 0)
        {
            if (startFind == -1)
                return -1;
            if (endFind == 0)
                endFind = data.Count;

            for (int i = startFind; i < endFind; i++)
            {
                string line = data[i];
                int idx = line.IndexOf(lblname);
                if ((idx) != -1)
                    return i;
            }
            /*
            MessageBox.Show(lblname + "が見つかりません。",
                            "エラー",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                            */
            return -1;
        }

        /// <summary>
        /// Listの内容を表示。
        /// </summary>
        public static void ShowListVariable(List<string> strs)
        {
            foreach (var a in strs)
                Console.WriteLine(a);
        }
    }
}
