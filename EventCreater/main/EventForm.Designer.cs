﻿namespace EventCreater
{
    partial class EventForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelEventName = new System.Windows.Forms.Label();
            this.textBoxEventName = new System.Windows.Forms.TextBox();
            this.labelAppearanceCondition = new System.Windows.Forms.Label();
            this.checkBoxFlag = new System.Windows.Forms.CheckBox();
            this.labelFlagTrue = new System.Windows.Forms.Label();
            this.labelGa = new System.Windows.Forms.Label();
            this.checkBoxVariable = new System.Windows.Forms.CheckBox();
            this.numericUpDownVariable = new System.Windows.Forms.NumericUpDown();
            this.comboBoxVariableCondition = new System.Windows.Forms.ComboBox();
            this.checkBoxMember = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxMember = new System.Windows.Forms.ComboBox();
            this.EventContents = new System.Windows.Forms.ListBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.編集ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.挿入ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.コピーToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.切り取りToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.貼り付けToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.削除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.buttonFlagCreater = new System.Windows.Forms.Button();
            this.buttonVariableCreater = new System.Windows.Forms.Button();
            this.labelFlag = new System.Windows.Forms.Label();
            this.buttonFlag = new System.Windows.Forms.Button();
            this.buttonVariable = new System.Windows.Forms.Button();
            this.labelVariable = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVariable)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelEventName
            // 
            this.labelEventName.AutoSize = true;
            this.labelEventName.Location = new System.Drawing.Point(23, 22);
            this.labelEventName.Name = "labelEventName";
            this.labelEventName.Size = new System.Drawing.Size(53, 12);
            this.labelEventName.TabIndex = 0;
            this.labelEventName.Text = "イベント名";
            // 
            // textBoxEventName
            // 
            this.textBoxEventName.Location = new System.Drawing.Point(25, 46);
            this.textBoxEventName.Name = "textBoxEventName";
            this.textBoxEventName.Size = new System.Drawing.Size(214, 19);
            this.textBoxEventName.TabIndex = 1;
            this.textBoxEventName.TextChanged += new System.EventHandler(this.SetFormParamToCondition);
            this.textBoxEventName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEventName_KeyDown);
            this.textBoxEventName.Leave += new System.EventHandler(this.SetFormParamToCondition);
            // 
            // labelAppearanceCondition
            // 
            this.labelAppearanceCondition.AutoSize = true;
            this.labelAppearanceCondition.Location = new System.Drawing.Point(25, 81);
            this.labelAppearanceCondition.Name = "labelAppearanceCondition";
            this.labelAppearanceCondition.Size = new System.Drawing.Size(89, 12);
            this.labelAppearanceCondition.TabIndex = 2;
            this.labelAppearanceCondition.Text = "イベント出現条件";
            // 
            // checkBoxFlag
            // 
            this.checkBoxFlag.AutoSize = true;
            this.checkBoxFlag.Location = new System.Drawing.Point(27, 105);
            this.checkBoxFlag.Name = "checkBoxFlag";
            this.checkBoxFlag.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBoxFlag.Size = new System.Drawing.Size(49, 16);
            this.checkBoxFlag.TabIndex = 3;
            this.checkBoxFlag.Text = "フラグ";
            this.checkBoxFlag.UseVisualStyleBackColor = true;
            this.checkBoxFlag.CheckedChanged += new System.EventHandler(this.checkBoxFlag_CheckedChanged);
            // 
            // labelFlagTrue
            // 
            this.labelFlagTrue.AutoSize = true;
            this.labelFlagTrue.Location = new System.Drawing.Point(188, 132);
            this.labelFlagTrue.Name = "labelFlagTrue";
            this.labelFlagTrue.Size = new System.Drawing.Size(38, 12);
            this.labelFlagTrue.TabIndex = 5;
            this.labelFlagTrue.Text = "がTrue";
            // 
            // labelGa
            // 
            this.labelGa.AutoSize = true;
            this.labelGa.Location = new System.Drawing.Point(25, 221);
            this.labelGa.Name = "labelGa";
            this.labelGa.Size = new System.Drawing.Size(15, 12);
            this.labelGa.TabIndex = 8;
            this.labelGa.Text = "が";
            // 
            // checkBoxVariable
            // 
            this.checkBoxVariable.AutoSize = true;
            this.checkBoxVariable.Location = new System.Drawing.Point(27, 166);
            this.checkBoxVariable.Name = "checkBoxVariable";
            this.checkBoxVariable.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBoxVariable.Size = new System.Drawing.Size(48, 16);
            this.checkBoxVariable.TabIndex = 6;
            this.checkBoxVariable.Text = "変数";
            this.checkBoxVariable.UseVisualStyleBackColor = true;
            this.checkBoxVariable.CheckedChanged += new System.EventHandler(this.checkBoxVariable_CheckedChanged);
            // 
            // numericUpDownVariable
            // 
            this.numericUpDownVariable.Enabled = false;
            this.numericUpDownVariable.Location = new System.Drawing.Point(46, 217);
            this.numericUpDownVariable.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numericUpDownVariable.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numericUpDownVariable.Name = "numericUpDownVariable";
            this.numericUpDownVariable.Size = new System.Drawing.Size(68, 19);
            this.numericUpDownVariable.TabIndex = 9;
            this.numericUpDownVariable.Enter += new System.EventHandler(this.SetFormParamToCondition);
            this.numericUpDownVariable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEventName_KeyDown);
            this.numericUpDownVariable.Leave += new System.EventHandler(this.SetFormParamToCondition);
            // 
            // comboBoxVariableCondition
            // 
            this.comboBoxVariableCondition.Enabled = false;
            this.comboBoxVariableCondition.FormattingEnabled = true;
            this.comboBoxVariableCondition.Items.AddRange(new object[] {
            "と同値",
            "以上",
            "以下",
            "より大きい",
            "より小さい"});
            this.comboBoxVariableCondition.Location = new System.Drawing.Point(134, 217);
            this.comboBoxVariableCondition.Name = "comboBoxVariableCondition";
            this.comboBoxVariableCondition.Size = new System.Drawing.Size(105, 20);
            this.comboBoxVariableCondition.TabIndex = 10;
            this.comboBoxVariableCondition.Enter += new System.EventHandler(this.SetFormParamToCondition);
            this.comboBoxVariableCondition.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEventName_KeyDown);
            this.comboBoxVariableCondition.Leave += new System.EventHandler(this.SetFormParamToCondition);
            // 
            // checkBoxMember
            // 
            this.checkBoxMember.AutoSize = true;
            this.checkBoxMember.Location = new System.Drawing.Point(27, 249);
            this.checkBoxMember.Name = "checkBoxMember";
            this.checkBoxMember.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBoxMember.Size = new System.Drawing.Size(61, 16);
            this.checkBoxMember.TabIndex = 11;
            this.checkBoxMember.Text = "メンバー";
            this.checkBoxMember.UseVisualStyleBackColor = true;
            this.checkBoxMember.CheckedChanged += new System.EventHandler(this.checkBoxMember_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(163, 274);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 12);
            this.label1.TabIndex = 13;
            this.label1.Text = "がいる。";
            // 
            // comboBoxMember
            // 
            this.comboBoxMember.Enabled = false;
            this.comboBoxMember.FormattingEnabled = true;
            this.comboBoxMember.Items.AddRange(new object[] {
            "アレスタ",
            "エリナ",
            "ペティル"});
            this.comboBoxMember.Location = new System.Drawing.Point(27, 271);
            this.comboBoxMember.Name = "comboBoxMember";
            this.comboBoxMember.Size = new System.Drawing.Size(121, 20);
            this.comboBoxMember.TabIndex = 12;
            this.comboBoxMember.Enter += new System.EventHandler(this.SetFormParamToCondition);
            this.comboBoxMember.Leave += new System.EventHandler(this.SetFormParamToCondition);
            // 
            // EventContents
            // 
            this.EventContents.FormattingEnabled = true;
            this.EventContents.ItemHeight = 12;
            this.EventContents.Location = new System.Drawing.Point(267, 32);
            this.EventContents.Name = "EventContents";
            this.EventContents.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.EventContents.Size = new System.Drawing.Size(588, 520);
            this.EventContents.TabIndex = 14;
            this.EventContents.DoubleClick += new System.EventHandler(this.EventContents_DoubleClick);
            this.EventContents.MouseDown += new System.Windows.Forms.MouseEventHandler(this.EventContents_MouseDown);
            this.EventContents.MouseUp += new System.Windows.Forms.MouseEventHandler(this.EventContents_MouseUp);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.編集ToolStripMenuItem,
            this.挿入ToolStripMenuItem,
            this.コピーToolStripMenuItem,
            this.切り取りToolStripMenuItem,
            this.貼り付けToolStripMenuItem,
            this.削除ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(116, 136);
            // 
            // 編集ToolStripMenuItem
            // 
            this.編集ToolStripMenuItem.Name = "編集ToolStripMenuItem";
            this.編集ToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.編集ToolStripMenuItem.Text = "編集";
            this.編集ToolStripMenuItem.Click += new System.EventHandler(this.編集ToolStripMenuItem_Click);
            // 
            // 挿入ToolStripMenuItem
            // 
            this.挿入ToolStripMenuItem.Name = "挿入ToolStripMenuItem";
            this.挿入ToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.挿入ToolStripMenuItem.Text = "挿入";
            this.挿入ToolStripMenuItem.Click += new System.EventHandler(this.挿入ToolStripMenuItem_Click);
            // 
            // コピーToolStripMenuItem
            // 
            this.コピーToolStripMenuItem.Name = "コピーToolStripMenuItem";
            this.コピーToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.コピーToolStripMenuItem.Text = "コピー";
            this.コピーToolStripMenuItem.Click += new System.EventHandler(this.コピーToolStripMenuItem_Click);
            // 
            // 切り取りToolStripMenuItem
            // 
            this.切り取りToolStripMenuItem.Name = "切り取りToolStripMenuItem";
            this.切り取りToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.切り取りToolStripMenuItem.Text = "切り取り";
            this.切り取りToolStripMenuItem.Click += new System.EventHandler(this.切り取りToolStripMenuItem_Click);
            // 
            // 貼り付けToolStripMenuItem
            // 
            this.貼り付けToolStripMenuItem.Name = "貼り付けToolStripMenuItem";
            this.貼り付けToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.貼り付けToolStripMenuItem.Text = "貼り付け";
            this.貼り付けToolStripMenuItem.Click += new System.EventHandler(this.貼り付けToolStripMenuItem_Click);
            // 
            // 削除ToolStripMenuItem
            // 
            this.削除ToolStripMenuItem.Name = "削除ToolStripMenuItem";
            this.削除ToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.削除ToolStripMenuItem.Text = "削除";
            this.削除ToolStripMenuItem.Click += new System.EventHandler(this.削除ToolStripMenuItem_Click);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(335, 568);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 15;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(534, 568);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 16;
            this.CancelButton.Text = "キャンセル";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ApplyButton
            // 
            this.ApplyButton.Location = new System.Drawing.Point(736, 568);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 17;
            this.ApplyButton.Text = "適用";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // buttonFlagCreater
            // 
            this.buttonFlagCreater.Location = new System.Drawing.Point(25, 336);
            this.buttonFlagCreater.Name = "buttonFlagCreater";
            this.buttonFlagCreater.Size = new System.Drawing.Size(104, 26);
            this.buttonFlagCreater.TabIndex = 18;
            this.buttonFlagCreater.Text = "フラグリスト";
            this.buttonFlagCreater.UseVisualStyleBackColor = true;
            this.buttonFlagCreater.Click += new System.EventHandler(this.buttonFlagCreater_Click);
            // 
            // buttonVariableCreater
            // 
            this.buttonVariableCreater.Location = new System.Drawing.Point(25, 381);
            this.buttonVariableCreater.Name = "buttonVariableCreater";
            this.buttonVariableCreater.Size = new System.Drawing.Size(104, 26);
            this.buttonVariableCreater.TabIndex = 19;
            this.buttonVariableCreater.Text = "変数リスト";
            this.buttonVariableCreater.UseVisualStyleBackColor = true;
            this.buttonVariableCreater.Click += new System.EventHandler(this.buttonVariableCreater_Click);
            // 
            // labelFlag
            // 
            this.labelFlag.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelFlag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelFlag.Enabled = false;
            this.labelFlag.Location = new System.Drawing.Point(27, 129);
            this.labelFlag.Margin = new System.Windows.Forms.Padding(3);
            this.labelFlag.Name = "labelFlag";
            this.labelFlag.Size = new System.Drawing.Size(121, 18);
            this.labelFlag.TabIndex = 20;
            this.labelFlag.Text = "0001:";
            this.labelFlag.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelFlag.TextChanged += new System.EventHandler(this.SetFormParamToCondition);
            // 
            // buttonFlag
            // 
            this.buttonFlag.Enabled = false;
            this.buttonFlag.Location = new System.Drawing.Point(148, 128);
            this.buttonFlag.Name = "buttonFlag";
            this.buttonFlag.Size = new System.Drawing.Size(30, 20);
            this.buttonFlag.TabIndex = 21;
            this.buttonFlag.Text = "...";
            this.buttonFlag.UseVisualStyleBackColor = true;
            this.buttonFlag.Click += new System.EventHandler(this.buttonFlag_Click);
            // 
            // buttonVariable
            // 
            this.buttonVariable.Enabled = false;
            this.buttonVariable.Location = new System.Drawing.Point(148, 187);
            this.buttonVariable.Name = "buttonVariable";
            this.buttonVariable.Size = new System.Drawing.Size(30, 20);
            this.buttonVariable.TabIndex = 23;
            this.buttonVariable.Text = "...";
            this.buttonVariable.UseVisualStyleBackColor = true;
            this.buttonVariable.Click += new System.EventHandler(this.buttonVariable_Click);
            // 
            // labelVariable
            // 
            this.labelVariable.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelVariable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelVariable.Enabled = false;
            this.labelVariable.Location = new System.Drawing.Point(27, 188);
            this.labelVariable.Margin = new System.Windows.Forms.Padding(3);
            this.labelVariable.Name = "labelVariable";
            this.labelVariable.Size = new System.Drawing.Size(121, 18);
            this.labelVariable.TabIndex = 22;
            this.labelVariable.Text = "0001:";
            this.labelVariable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelVariable.TextChanged += new System.EventHandler(this.SetFormParamToCondition);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 20;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // EventForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 599);
            this.Controls.Add(this.buttonVariable);
            this.Controls.Add(this.labelVariable);
            this.Controls.Add(this.buttonFlag);
            this.Controls.Add(this.labelFlag);
            this.Controls.Add(this.buttonVariableCreater);
            this.Controls.Add(this.buttonFlagCreater);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.EventContents);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxMember);
            this.Controls.Add(this.checkBoxMember);
            this.Controls.Add(this.comboBoxVariableCondition);
            this.Controls.Add(this.numericUpDownVariable);
            this.Controls.Add(this.labelGa);
            this.Controls.Add(this.checkBoxVariable);
            this.Controls.Add(this.labelFlagTrue);
            this.Controls.Add(this.checkBoxFlag);
            this.Controls.Add(this.labelAppearanceCondition);
            this.Controls.Add(this.textBoxEventName);
            this.Controls.Add(this.labelEventName);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EventForm";
            this.Text = "イベント作成";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EventForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVariable)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelEventName;
        private System.Windows.Forms.TextBox textBoxEventName;
        private System.Windows.Forms.Label labelAppearanceCondition;
        private System.Windows.Forms.CheckBox checkBoxFlag;
        private System.Windows.Forms.Label labelFlagTrue;
        private System.Windows.Forms.Label labelGa;
        private System.Windows.Forms.CheckBox checkBoxVariable;
        private System.Windows.Forms.NumericUpDown numericUpDownVariable;
        private System.Windows.Forms.ComboBox comboBoxVariableCondition;
        private System.Windows.Forms.CheckBox checkBoxMember;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxMember;
        private System.Windows.Forms.Button OKButton;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button buttonFlagCreater;
        private System.Windows.Forms.Button buttonVariableCreater;
        private System.Windows.Forms.Label labelFlag;
        private System.Windows.Forms.Button buttonFlag;
        private System.Windows.Forms.Button buttonVariable;
        private System.Windows.Forms.Label labelVariable;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 編集ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 挿入ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 削除ToolStripMenuItem;
        public System.Windows.Forms.ListBox EventContents;
        private System.Windows.Forms.ToolStripMenuItem コピーToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 切り取りToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 貼り付けToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
    }
}