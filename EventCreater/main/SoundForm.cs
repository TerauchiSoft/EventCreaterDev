﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class SoundForm : Form
    {
        /// <summary>
        /// 0でbgm, 1でse
        /// </summary>
        int isSe = 0;

        int[] Args = new int[4];
        EventContentsForm ecf;
        public SoundForm(int s_mode, Form parent, int fm_mode, string s_St = null, int[] e_Args = null)
        {
            InitializeComponent();
            isSe = s_mode;
            groupBox2.Enabled = (isSe == 0) ? true : false;
            groupBox2.Visible = (isSe == 0) ? true : false;

            Sound.ReadSoundFile(Sound.fname[s_mode]);
            listBox1.Items.AddRange(Sound.soundName.ToArray());
            listBox2.Items.AddRange(Sound.soundUse.ToArray());
            listBox3.Items.AddRange(Sound.soundLength.ToArray());
            listBox1.SelectedIndex = 0;
            listBox2.SelectedIndex = 0;
            listBox3.SelectedIndex = 0;

            if (fm_mode < 2)
            {
                buttonOK.Enabled = true;
                buttonOK.Visible = true;
            }
            else
            {
                fm_mode = 0;
            }
            ecf = new EventContentsForm(this, fm_mode);
            SetArgsToForm(s_St, e_Args);
        }

        private void SetArgsToForm(string st, int[] ags)
        {
            if (ags == null) return;
            if (isSe == 0)
            {
                trackBar1.Value = ags[0];
                trackBar2.Value = ags[1];
                trackBar3.Value = ags[2];
                trackBar4.Value = ags[3];
                st = st.Remove(0, 4);
            }
            else
            {
                trackBar2.Value = ags[0];
                trackBar3.Value = ags[1];
                trackBar4.Value = ags[2];
            }
            for (int i = 0; i < Sound.soundPath.Count; i++)
                if (Sound.soundPath[i] == st)
                {
                    listBox1.SelectedIndex = i;
                    break;
                }

            listBox2.TopIndex = listBox1.TopIndex;
            listBox3.TopIndex = listBox1.TopIndex;
            listBox2.SelectedIndex = listBox1.SelectedIndex;
            listBox3.SelectedIndex = listBox1.SelectedIndex;
            clckbox = 0;
        }

        int clckbox = 0;
        private void listBox1_MouseDown(object sender, MouseEventArgs e)
        {
            listBox2.TopIndex = listBox1.TopIndex;
            listBox3.TopIndex = listBox1.TopIndex;
            listBox2.SelectedIndex = listBox1.SelectedIndex;
            listBox3.SelectedIndex = listBox1.SelectedIndex;
            clckbox = 0;
        }

        private void listBox2_MouseDown(object sender, MouseEventArgs e)
        {
            listBox1.TopIndex = listBox2.TopIndex;
            listBox3.TopIndex = listBox2.TopIndex;
            listBox1.SelectedIndex = listBox2.SelectedIndex;
            listBox3.SelectedIndex = listBox2.SelectedIndex;
            clckbox = 1;
        }

        private void listBox3_MouseDown(object sender, MouseEventArgs e)
        {
            listBox1.TopIndex = listBox3.TopIndex;
            listBox2.TopIndex = listBox3.TopIndex;
            listBox1.SelectedIndex = listBox3.SelectedIndex;
            listBox2.SelectedIndex = listBox3.SelectedIndex;
            clckbox = 2;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            string name = Sound.soundPath[listBox1.SelectedIndex];
            if (isSe == 0) name = "bgm\\" + name;
            Args[0] = trackBar1.Value;
            Args[1] = trackBar2.Value;
            Args[2] = trackBar3.Value;
            Args[3] = trackBar4.Value;

            string[] code = new string[1];
            if (isSe == 0)
            {
                code[0] = "PlayBGM(" + "\"" + name + "\"," + Args[0] + "," + Args[1] + "," + Args[2] + "," + Args[3] + ")";
            }
            else
            { 
                code[0] = "PlaySE(" + "\"" + name + "\"," + Args[1] + "," + Args[2] + "," + Args[3] + ")";
            }
            ecf.buttonOK_Close(code);
        }

        private void buttonCansel_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        //--------------Sound再生にはMCIを使う--------------------

        [System.Runtime.InteropServices.DllImport("winmm.dll")]
        private static extern int mciSendString(String command,
                                                StringBuilder buffer, 
                                                int bufferSize, 
                                                IntPtr hwndCallback);

        private string aliasName = "MediaFile";

        private void button1_Click(object sender, EventArgs e)
        {
            StopSound();
            PlaySound();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            StopSound();
        }

        private string GetFileName()
        {
            string pt = Application.ExecutablePath;
            string[] rmst = pt.Split('\\');
            int id = pt.IndexOf(rmst[rmst.Length - 1]);
            pt = pt.Remove(id, rmst[rmst.Length - 1].Length);
            string mpt = "";
            if (isSe == 0)
                mpt += "bgm\\";

            //再生するファイル名
            string fileName = pt + mpt + Sound.soundPath[listBox1.SelectedIndex];// "C:\\music.mp3";
            return fileName;
        }

        private void PlaySound()
        {
            string fileName = GetFileName();

            string cmd;
            //ファイルを開く
            cmd = "open \"" + fileName + "\" type mpegvideo alias " + aliasName;
            if (mciSendString(cmd, null, 0, IntPtr.Zero) != 0)
                return;

            //再生する
            cmd = "play " + aliasName;
            mciSendString(cmd, null, 0, IntPtr.Zero);
            SpeedSet();
            VolumeSet();
        }

        private void StopSound()
        {
            string cmd;
            //再生しているWAVEを停止する
            cmd = "stop " + aliasName;
            mciSendString(cmd, null, 0, IntPtr.Zero);
            //閉じる
            cmd = "close " + aliasName;
            mciSendString(cmd, null, 0, IntPtr.Zero);
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            VolumeSet();
        }

        private void VolumeSet()
        {
            int vol = trackBar2.Value * 100; // 0～1000 の範囲を指定

            // 音量変更
            string cmd = "setaudio " + aliasName + " volume to " + vol;
            mciSendString(cmd, null, 0, IntPtr.Zero);
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            SpeedSet();
        }

        private void SpeedSet()
        {
            int spd = trackBar3.Value * 100;

            // speed変更
            string cmd = "set " + aliasName + " speed " + spd;
            mciSendString(cmd, null, 0, IntPtr.Zero);
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            StopSound();
            PlaySound();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == listBox2.SelectedIndex && listBox2.SelectedIndex == listBox3.SelectedIndex &&
                listBox1.TopIndex == listBox2.TopIndex && listBox2.TopIndex == listBox3.TopIndex)
                return;

            if (clckbox == 0)
            {
                listBox2.TopIndex = listBox1.SelectedIndex;
                listBox3.TopIndex = listBox1.SelectedIndex;
                listBox2.TopIndex = listBox1.TopIndex;
                listBox3.TopIndex = listBox1.TopIndex;
            }
            if (clckbox == 1)
            {
                listBox1.TopIndex = listBox2.SelectedIndex;
                listBox3.TopIndex = listBox2.SelectedIndex;
                listBox1.TopIndex = listBox2.TopIndex;
                listBox3.TopIndex = listBox2.TopIndex;
            }
            if (clckbox == 2)
            {
                listBox2.TopIndex = listBox3.SelectedIndex;
                listBox1.TopIndex = listBox3.SelectedIndex;
                listBox2.TopIndex = listBox3.TopIndex;
                listBox1.TopIndex = listBox3.TopIndex;
            }
        }
    }
}
