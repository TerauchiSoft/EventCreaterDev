﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace EventCreater
{
    public partial class EventList : Form
    {
        /// <summary>
        /// 表示するデータバッファ、イベントのタイトル
        /// </summary>
        private string[] viewBuf;

        /// <summary>
        /// 内部データ用のラベルバッファ
        /// イベントを編集するときに使用。
        /// </summary>
        private string[] labels;
        

        public EventList()
        {
            AppPath.SetAppPath();
            InitializeComponent();
        }

        /// <summary>
        /// フォームロード時にイベントリストの読み込み
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EventList_Load(object sender, EventArgs e)
        {
            EventData.LoadEventListFormFile();
            ViewEventList();
        }

        /// <summary>
        /// 有効化時に再度ロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EventList_EnabledChanged(object sender, EventArgs e)
        {
            var form = (Form)sender;
            if (form.Enabled)
            {
                EventData.LoadEventListFormFile();
                ViewEventList();
            }
        }

        /// <summary>
        /// イベントリストの表示
        /// srcBuf -> viewBuf
        /// </summary>
        private void ViewEventList()
        {
            var ars = EventData.srcBuf[0].Split(':');
            EventData.NumEvent = int.Parse(ars[1]);

            EventData.BufToEventNameAndLabels(ref viewBuf, ref labels);
            ViewBufViewToListBox(viewBuf);
        }

        /// <summary>
        /// リストボックスにイベントを表示
        /// </summary>
        private void ViewBufViewToListBox(string[] dat)
        {
            ListBoxShow.ShowDataInListBox(listBoxMainEvent, dat);
        }

        /// <summary>
        /// イベント項目ダブルクリックでイベント編集
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxMainEvent_DoubleClick(object sender, EventArgs e)
        {   /* 参照用
            ListBox lb = (ListBox)sender;
            // SelectedItem = 文字値
            // SelectedIndex = 0 ~ n
            Console.WriteLine(lb.SelectedIndex);
            // mea.Location = マウス座標
            MouseEventArgs mea = (MouseEventArgs)e;
            Console.WriteLine(mea.Location);
            */
            ListBox lb = (ListBox)sender;
            if (lb.SelectedIndex != -1 && lb.SelectedIndex < lb.Items.Count)
            {
                EventForm ef = new EventForm(labels[lb.SelectedIndex]);
                CloseAct.FormOpenCommon(this, ef);
            }
        }

        /// <summary>
        /// イベント作成フォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEventCreater_Click(object sender, EventArgs e)
        {
            EventForm ef = new EventForm();
            CloseAct.FormOpenCommon(this, ef);
        }

        /// <summary>
        /// フラグ作成フォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFlagCreater_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(0);
            CloseAct.FormOpenCommon(this, vf);
        }

        /// <summary>
        /// 変数作成フォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonVariableCreater_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1);
            CloseAct.FormOpenCommon(this, vf);
        }

        /// <summary>
        /// アイテムリストフォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonItemCreater_Click(object sender, EventArgs e)
        {
            ItemForm itf = new ItemForm(0);
            CloseAct.FormOpenCommon(this, itf);
        }

        /// <summary>
        /// スキルリストフォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSkillList_Click(object sender, EventArgs e)
        {
            ItemForm skf = new ItemForm(1);
            CloseAct.FormOpenCommon(this, skf);
        }

        /// <summary>
        /// 右クリック時の操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void contextMenuStrip1_Opened(object sender, EventArgs e)
        {
            var pos = Cursor.Position;
            var ctext = (ContextMenuStrip)sender;
            if (listBoxMainEvent.SelectedIndex != -1)
            {
                ctext.Enabled = true;
            }
            else
            {
                ctext.Enabled = false;
            }
        }
        

        /// <summary>
        /// BGMフォーム
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            Form fm = new SoundForm(0, null, 2);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// 効果音フォーム
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            Form fm = new SoundForm(1, null, 2);
            CloseAct.FormOpenCommon(this, fm);
        }

        /// <summary>
        /// キャラ名編集追加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(5);
            CloseAct.FormOpenCommon(this, vf);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(2);
            CloseAct.FormOpenCommon(this, vf);
        }
    }
}
