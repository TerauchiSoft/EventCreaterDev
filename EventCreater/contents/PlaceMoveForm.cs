﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class PlaceMoveForm : Form
    {
        EventContentsForm ecf;
        ComponentsSync arsyn;
        public PlaceMoveForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            InitializeComponent();
            SetArgs(e_Args);


            object[] radios = 
            { 
                radioButton1, radioButton1, radioButton1,
                radioButton2, radioButton2, radioButton2,
                radioButton2, radioButton2, radioButton2
            };

            object[] boxs = 
            {
                numericUpDown1, numericUpDown2, numericUpDown3,
                labelfl, labelx, labely,
                buttonval, button1, button2
            };

            arsyn = new ComponentsSync(radios, boxs);
            arsyn.async();
            ecf = new EventContentsForm(this, mode);
        }
        
        private void SetArgs(int[] args)
        {
            if (args == null)
                return;

            if (args[0] == 0)
            {
                radioButton1.Checked = true;
                numericUpDown1.Value = args[1];
                numericUpDown2.Value = args[2];
                numericUpDown3.Value = args[3];
            }
            else
            {
                radioButton2.Checked = true;
                labelfl.Text = Variable.GetVariableLabelText(args[1]);
                labelx.Text = Variable.GetVariableLabelText(args[2]);
                labely.Text = Variable.GetVariableLabelText(args[3]);
            }
        }

        private void buttonval_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, (int a, string vnamecomp) =>
            {
                labelfl.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, (int a, string vnamecomp) =>
            {
                labelx.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, (int a, string vnamecomp) =>
            {
                labely.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            int select = (radioButton2.Checked == true) ? 1 : 0;
            int ag1;
            int ag2;
            int ag3;
            if (select == 0)
            {
                ag1 = (int)numericUpDown1.Value;
                ag2 = (int)numericUpDown2.Value;
                ag3 = (int)numericUpDown3.Value;
            }
            else
            {
                ag1 = Variable.GetVariableIndex(labelfl);
                ag2 = Variable.GetVariableIndex(labelx);
                ag3 = Variable.GetVariableIndex(labely);
            }

            string[] code = { "MovePlace(" + select + "," + ag1 + "," + ag2 + "," + ag3 + ")" };
            ecf.buttonOK_Close(code);
        }

        private void buttonCansel_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                buttonOK_Click(null, null);
        }
    }
}
